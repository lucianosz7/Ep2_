package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controllers.AcaoAprendaQee;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.GridBagLayout;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;
import javax.swing.JList;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.awt.Color;


public class AprendaQee extends JFrame {

	private JPanel MenuPanel;


	public AprendaQee() {
		setResizable(false);
		setFont(new Font("FreeSans", Font.PLAIN, 20));
		setTitle("AprendaQee");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 859, 496);
		MenuPanel = new JPanel();
		MenuPanel.setBackground(Color.WHITE);
		MenuPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(MenuPanel);
		MenuPanel.setLayout(null);

		JLabel lblWarnings = new JLabel("Warnings");
		lblWarnings.setHorizontalAlignment(SwingConstants.CENTER);
		lblWarnings.setBounds(22, 425, 815, 19);
		MenuPanel.add(lblWarnings);

		JButton btnFluxoDePotncia = new JButton("Fluxo de Potência Harmonica");
		btnFluxoDePotncia.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		


		JButton btnNewButton = new JButton("Fluxo de Potência Fundamental");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});
		btnNewButton.setBounds(25, 340, 256, 25);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String command = arg0.getActionCommand();
				if(command.equals("Fluxo de Potência Fundamental")) {
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								FluxoPotenciaFundamental frame = new FluxoPotenciaFundamental();
								frame.setVisible(true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
					lblWarnings.setText("Chamando função FPF");
				}
			}
		});

		JButton btnDistoroHarmonica = new JButton("Distorção Harmonica");
		btnDistoroHarmonica.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		btnDistoroHarmonica.setBounds(320, 340, 191, 25);
		MenuPanel.add(btnDistoroHarmonica);
		MenuPanel.add(btnNewButton);
		btnDistoroHarmonica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String command = arg0.getActionCommand();
				if(command.equals("Distorção Harmonica")) {
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								DistorcaoHarmonica frame = new DistorcaoHarmonica();
								frame.setVisible(true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

				});
				lblWarnings.setText("Chamando função DH");
			}
		}

		});




		JButton btnSair = new JButton("Sair");
		btnSair.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		btnSair.setBounds(22, 388, 825, 25);
		MenuPanel.add(btnSair);
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String command = arg0.getActionCommand();
				if(command.equals("Sair")) {
					lblWarnings.setText("Saindo");
					System.exit(0);
				}
			}
		});

		JTextPane txtpnSelecioneAOpo = new JTextPane();
		txtpnSelecioneAOpo.setFont(new Font("SansSerif", Font.BOLD, 16));
		txtpnSelecioneAOpo.setText("Selecione a opção desejada para começar a simulação.");
		txtpnSelecioneAOpo.setBounds(181, 303, 496, 25);
		MenuPanel.add(txtpnSelecioneAOpo);
		txtpnSelecioneAOpo.setEditable(false);

		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon("/home/luciano/Documentos/UnB/3ºSemestre/OO/Java/PROJETOS/Ep2_/Imagens/Paraboloidematlab2009.png"));
		label.setBounds(17, 27, 825, 264);
		MenuPanel.add(label);







	}
}
