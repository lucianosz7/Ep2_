package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JProgressBar;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.SpinnerNumberModel;
import java.awt.Window.Type;
import java.awt.Dialog.ModalExclusionType;

import controllers.AcaoInterfaceDistorcaoHarmonica;
import model.GraphPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

public class DistorcaoHarmonica extends JFrame {

	public static JLabel ComponenteFun_panel;
	public static JPanel Harmonica_panel1;
	public static JPanel contentPane;
	public static JTextField SerieFourier_Field;
	private int Impar;
	private int Par;
	private Integer value = new Integer(0);
	private Integer min = new Integer(-180);
	private Integer max = new Integer(180);
	private Integer step = new Integer(1);




	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public DistorcaoHarmonica() throws IOException {
		setAutoRequestFocus(false);
		setFont(new Font("Linux Libertine Display O", Font.BOLD, 16));
		setBackground(Color.WHITE);
		setForeground(Color.WHITE);
		setTitle("Distorção Harmônica ");
		setAlwaysOnTop(true);
		setBounds(100, 100, 894, 934);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel ComponenteFun_panel = new JLabel("");
		ComponenteFun_panel.setBounds(17, 38, 384, 136);
		contentPane.add(ComponenteFun_panel);
		
		JLabel label = new JLabel("Entradas");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Liberation Sans", Font.BOLD, 22));
		label.setBounds(344, 0, 132, 26);
		contentPane.add(label);
		
		JTextPane txtpnComponente = new JTextPane();
		txtpnComponente.setFont(new Font("FreeSans", Font.PLAIN, 14));
		txtpnComponente.setText("Componente Fundamental");
		txtpnComponente.setBounds(121, 12, 176, 36);
		contentPane.add(txtpnComponente);
		txtpnComponente.setEditable(false);
		
		JTextPane txtpnAmplitude = new JTextPane();
		txtpnAmplitude.setText("Amplitude");
		txtpnAmplitude.setBounds(412, 38, 74, 21);
		contentPane.add(txtpnAmplitude);
		txtpnAmplitude.setEditable(false);
		
		JTextPane textPane = new JTextPane();
		textPane.setText("Ângulo de Fase [ θº]");
		textPane.setEditable(false);
		textPane.setBounds(552, 38, 173, 21);
		contentPane.add(textPane);
		textPane.setEditable(false);
		
		JProgressBar progressAmp_Har1 = new JProgressBar();
		progressAmp_Har1.setValue(55);
		progressAmp_Har1.setOrientation(SwingConstants.VERTICAL);
		progressAmp_Har1.setBounds(434, 63, 30, 94);
		progressAmp_Har1.setMaximum(220);
		progressAmp_Har1.setMinimum(0);
		contentPane.add(progressAmp_Har1);
		
		JSpinner Angulo_sp = new JSpinner();
		Angulo_sp.setModel(new SpinnerNumberModel(value,min,max,step));
		Angulo_sp.setBounds(562, 63, 112, 26);
		contentPane.add(Angulo_sp);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(Color.BLACK);
		separator.setBounds(0, 200, 894, 21);
		contentPane.add(separator);
		
		JTextPane txtpnHarmnicos = new JTextPane();
		txtpnHarmnicos.setText("Harmônicos");
		txtpnHarmnicos.setBounds(768, 222, 86, 26);
		contentPane.add(txtpnHarmnicos);
		txtpnHarmnicos.setEditable(false);
		
		JPanel Harmonica_panel1 = new JPanel();
		
		Harmonica_panel1.setBackground(Color.CYAN);
		
		ButtonGroup ParOrImpButtonGroup = new ButtonGroup();
		
		JRadioButtonMenuItem Select_Impar = new JRadioButtonMenuItem("Ímpares");
		Select_Impar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Impar = 1;
				System.out.println(Impar);
			}
		});
		Select_Impar.setBackground(Color.WHITE);
		Select_Impar.setBounds(768, 246, 91, 19);
		contentPane.add(Select_Impar);
		ParOrImpButtonGroup.add(Select_Impar);
		
		JRadioButtonMenuItem Select_Par = new JRadioButtonMenuItem("Pares");
		Select_Par.setBackground(Color.WHITE);
		Select_Par.setBounds(768, 269, 86, 19);
		contentPane.add(Select_Par);
		Select_Par.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Par = 2;
				System.out.println(Par);
			}
		});
		ParOrImpButtonGroup.add(Select_Par);
		
		JSpinner NumOrdHarmonicas = new JSpinner();
		NumOrdHarmonicas.setModel(new SpinnerNumberModel(0, 0, 6, 1));
		NumOrdHarmonicas.setBounds(592, 258, 112, 26);
		contentPane.add(NumOrdHarmonicas);
		
		JTextPane txtpnNmeroDeOrde = new JTextPane();
		txtpnNmeroDeOrde.setFont(new Font("FreeSans", Font.PLAIN, 12));
		txtpnNmeroDeOrde.setText("Número de ordens Harmônicas");
		txtpnNmeroDeOrde.setBounds(562, 225, 206, 21);
		contentPane.add(txtpnNmeroDeOrde);
		txtpnNmeroDeOrde.setEditable(false);
		
		
		/*JPanel Harmonica_panel2 = new JPanel();
		Harmonica_panel2.setBackground(Color.CYAN);
		Harmonica_panel2.setBounds(17, 428, 384, 119);
		contentPane.add(Harmonica_panel2);*/
		
		JTextPane txtpnHarmnicas = new JTextPane();
		txtpnHarmnicas.setFont(new Font("FreeSans", Font.PLAIN, 14));
		txtpnHarmnicas.setText("Harmônicas");
		txtpnHarmnicas.setBounds(166, 244, 86, 21);
		contentPane.add(txtpnHarmnicas);
		txtpnHarmnicas.setEditable(false);
		
		JProgressBar progressAmp_Har2 = new JProgressBar();
		progressAmp_Har2.setValue(65);
		progressAmp_Har2.setOrientation(SwingConstants.VERTICAL);
		progressAmp_Har2.setBounds(434, 294, 30, 94);
		progressAmp_Har2.setMaximum(220);
		progressAmp_Har2.setMinimum(0);
		contentPane.add(progressAmp_Har2);
		
		JTextPane AmplitudeText1 = new JTextPane();
		AmplitudeText1.setText("Amplitude");
		AmplitudeText1.setBounds(412, 269, 74, 21);
		contentPane.add(AmplitudeText1);
		AmplitudeText1.setEditable(false);
		
		JProgressBar progressAmp_Har3 = new JProgressBar();
		progressAmp_Har3.setValue(15);
		progressAmp_Har3.setOrientation(SwingConstants.VERTICAL);
		progressAmp_Har3.setBounds(434, 453, 30, 94);
		progressAmp_Har3.setMaximum(220);
		progressAmp_Har3.setMinimum(0);
		contentPane.add(progressAmp_Har3);
		
		JTextPane AmplitudeText2 = new JTextPane();
		AmplitudeText2.setText("Amplitude");
		AmplitudeText2.setBounds(412, 428, 74, 21);
		contentPane.add(AmplitudeText2);
		AmplitudeText2.setEditable(false);
		
		JSpinner Angulo_sp2 = new JSpinner();
		Angulo_sp2.setModel(new SpinnerNumberModel(value,min,max,step));
		Angulo_sp2.setBounds(543, 336, 112, 26);
		contentPane.add(Angulo_sp2);
		
		JTextPane AnguloFase_1 = new JTextPane();
		AnguloFase_1.setText("Ângulo de Fase [ θº]");
		AnguloFase_1.setEditable(false);
		AnguloFase_1.setBounds(514, 303, 141, 21);
		contentPane.add(AnguloFase_1);
		AnguloFase_1.setEditable(false);
		
		JSpinner OrdemHar_sp = new JSpinner();
		OrdemHar_sp.setModel(new SpinnerNumberModel(0, 0, 15, 1));
		OrdemHar_sp.setBounds(716, 336, 112, 26);
		contentPane.add(OrdemHar_sp);
		
		JTextPane txtpnOrdemHarmnica2 = new JTextPane();
		txtpnOrdemHarmnica2.setFont(new Font("FreeSans", Font.PLAIN, 12));
		txtpnOrdemHarmnica2.setText("Ordem Harmônica");
		txtpnOrdemHarmnica2.setBounds(711, 303, 123, 21);
		contentPane.add(txtpnOrdemHarmnica2);
		txtpnOrdemHarmnica2.setEditable(false);
		
		JSpinner Angulo_Sp3 = new JSpinner();
		Angulo_Sp3.setModel(new SpinnerNumberModel(value,min,max,step));
		Angulo_Sp3.setBounds(543, 461, 112, 26);
		contentPane.add(Angulo_Sp3);
		
		JTextPane AnguloFase3 = new JTextPane();
		AnguloFase3.setText("Ângulo de Fase [ θº]");
		AnguloFase3.setEditable(false);
		AnguloFase3.setBounds(514, 428, 141, 21);
		contentPane.add(AnguloFase3);
		AnguloFase3.setEditable(false);
		
		JSpinner OrdemHar_sp2 = new JSpinner();
		OrdemHar_sp2.setModel(new SpinnerNumberModel(0, 0, 15, 1));
		OrdemHar_sp2.setBounds(716, 461, 112, 26);
		contentPane.add(OrdemHar_sp2);
		
		
		JTextPane txtpnOrdemHarmnica3 = new JTextPane();
		txtpnOrdemHarmnica3.setText("Ordem Harmônica");
		txtpnOrdemHarmnica3.setFont(new Font("FreeSans", Font.PLAIN, 12));
		txtpnOrdemHarmnica3.setBounds(711, 428, 123, 21);
		contentPane.add(txtpnOrdemHarmnica3);
		txtpnOrdemHarmnica3.setEditable(false);
	
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBackground(Color.BLACK);
		separator_1.setBounds(0, 669, 894, 21);
		contentPane.add(separator_1);
		
		JTextPane txtpnSaidas = new JTextPane();
		txtpnSaidas.setFont(new Font("Liberation Sans", Font.BOLD, 22));
		txtpnSaidas.setText("Saídas");
		txtpnSaidas.setBounds(365, 669, 86, 36);	
		contentPane.add(txtpnSaidas);
		txtpnSaidas.setEditable(false);
		
		JPanel Resultante_panel = new JPanel();
		Resultante_panel.setBounds(17, 717, 384, 119);
		contentPane.add(Resultante_panel);
		
		JTextPane txtpnResultante = new JTextPane();
		txtpnResultante.setFont(new Font("FreeSans", Font.PLAIN, 14));
		txtpnResultante.setText("Resultante");
		txtpnResultante.setBounds(172, 689, 74, 21);
		contentPane.add(txtpnResultante);
		txtpnResultante.setEditable(false);
		
		JTextPane SerieFourier = new JTextPane();
		SerieFourier.setFont(new Font("SansSerif", Font.PLAIN, 12));
		SerieFourier.setText("Série de Fourier Amplitude-Fase");
		SerieFourier.setBounds(576, 717, 216, 21);
		contentPane.add(SerieFourier);
		SerieFourier.setEditable(false);
		
		SerieFourier_Field = new JTextField();
		SerieFourier_Field.setBounds(541, 750, 287, 19);
		contentPane.add(SerieFourier_Field);
		SerieFourier_Field.setColumns(10);
		SerieFourier_Field.setEditable(false);
		
		List<Double> scores = new ArrayList<>();
		int maxDataPoints = 50;
		GraphPanel ComponenteFundamentalPane = new model.GraphPanel(scores);
		ComponenteFundamentalPane.setOpaque(false);
		ComponenteFundamentalPane.setBorder(null);
		ComponenteFundamentalPane.setBounds(17, 38, 384, 136);
		ComponenteFundamentalPane.setBackground(new Color(238,238,238));
		//ComponenteFundamentalPane.setVisible(false);
		contentPane.add(ComponenteFundamentalPane);
		
		List<Double> scores2 = new ArrayList<>();
		GraphPanel PanelHarm1 = new model.GraphPanel(scores2);
		PanelHarm1.setOpaque(false);
		PanelHarm1.setBorder(null);
		PanelHarm1.setBounds(17, 269, 384, 136);
		PanelHarm1.setBackground(new Color(238,238,238));
		PanelHarm1.setVisible(false);
		contentPane.add(PanelHarm1);
		
		List<Double> scores3 = new ArrayList<>();
		GraphPanel PanelHarm2 = new model.GraphPanel(scores3);
		PanelHarm2.setOpaque(false);
		PanelHarm2.setBorder(null);
		PanelHarm2.setBounds(17, 417, 384, 136);
		PanelHarm2.setBackground(new Color(238,238,238));
		PanelHarm2.setVisible(false);
		contentPane.add(PanelHarm2);
		
		List<Double> scores4 = new ArrayList<>();
		GraphPanel PanelResultante = new model.GraphPanel(scores4);
		PanelResultante.setOpaque(false);
		PanelResultante.setBorder(null);
		PanelResultante.setBounds(17, 717, 384, 119);
		PanelResultante.setBackground(new Color(238,238,238));
		PanelResultante.setVisible(false);
		contentPane.add(PanelResultante);
		
		//ComponenteFundamentalPane.setVisible(false);
		Angulo_sp2.setVisible(false);
		OrdemHar_sp.setVisible(false);
		Angulo_Sp3.setVisible(false);
		OrdemHar_sp2.setVisible(false);
		AnguloFase_1.setVisible(false);
		AnguloFase3.setVisible(false);
		txtpnOrdemHarmnica2.setVisible(false);
		txtpnOrdemHarmnica3.setVisible(false);
		progressAmp_Har2.setVisible(false);
		progressAmp_Har3.setVisible(false);
		AmplitudeText1.setVisible(false);
		AmplitudeText2.setVisible(false);
		
		
		
		
		
		JButton buttonOk = new JButton("Ok");
		buttonOk.setBounds(780, 522, 74, 25);
		
		 buttonOk.addActionListener(new AcaoInterfaceDistorcaoHarmonica(Angulo_sp, Angulo_sp2,Angulo_Sp3,OrdemHar_sp, OrdemHar_sp2, NumOrdHarmonicas, ComponenteFundamentalPane,PanelHarm1,PanelHarm2,PanelResultante,txtpnOrdemHarmnica2,txtpnOrdemHarmnica3,progressAmp_Har2,progressAmp_Har3,AmplitudeText1,AmplitudeText2,AnguloFase_1,AnguloFase3,maxDataPoints));
	
		contentPane.add(buttonOk);
		

		
	
				
	}
}
