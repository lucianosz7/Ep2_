package view;

import java.awt.EventQueue;
import java.io.IOException;

public class Main {
  public static void main(String[] args) throws IOException {
	  EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AprendaQee frame = new AprendaQee();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
  }

}
