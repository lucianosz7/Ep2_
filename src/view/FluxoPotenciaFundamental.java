package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jfree.ui.RefineryUtilities;

import java.io.File;

import javax.swing.DebugGraphics;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Panel;

import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpinnerListModel;
import java.awt.event.InputMethodListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputMethodEvent;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JTextPane;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import java.awt.Choice;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Cursor;
import java.lang.Integer;
import java.util.ArrayList;
import java.util.List;
import java.awt.CardLayout;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Label;

import controllers.AcaoInterfaceFluxoFundamental;
import model.GraphPanel;

public class FluxoPotenciaFundamental extends JFrame {


	private JPanel FluxoFundamentalPane;
	public static JTextField Text_Pont_Atv;
	public static JTextField Text_Pot_Rea;
	public static JTextField Text_Pot_Apa;
	public static JTextField Text_Fator_Pot;
	private double Potencia_Ativa;
	private double Potencia_Reativa;
	private double Potencia_Aparente;
	private double Fator_Potencia;



	/**
	 * Create the frame.
	 */
	public FluxoPotenciaFundamental() {
		setFont(new Font("FreeSans", Font.PLAIN, 14));
		setTitle("Fluxo de Potência Fundamental");
		setResizable(false);
		setForeground(Color.WHITE);
		setBackground(Color.WHITE);
		setBounds(100, 100, 902, 722);
		FluxoFundamentalPane = new JPanel();
		FluxoFundamentalPane.setBackground(Color.WHITE);
		FluxoFundamentalPane.setForeground(Color.WHITE);
		FluxoFundamentalPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(FluxoFundamentalPane);
		FluxoFundamentalPane.setLayout(null);

		JLabel lblEntrada = new JLabel("Entradas");
		lblEntrada.setFont(new Font("Liberation Sans", Font.BOLD, 22));
		lblEntrada.setHorizontalAlignment(SwingConstants.CENTER);
		lblEntrada.setBounds(385, 0, 132, 26);
		FluxoFundamentalPane.add(lblEntrada);

		Integer value = new Integer(0);
		Integer min = new Integer(-180);
		Integer max = new Integer(180);
		Integer step = new Integer(1);

		JSpinner Angulo_sp = new JSpinner();
		Angulo_sp.setModel(new SpinnerNumberModel(value,min,max,step));
		Angulo_sp.setBounds(754, 48, 93, 26);
		FluxoFundamentalPane.add(Angulo_sp);

		JProgressBar progress_veff = new JProgressBar();
		progress_veff.setMaximum(220);
		progress_veff.setOrientation(SwingConstants.VERTICAL);
		progress_veff.setStringPainted(true);
		progress_veff.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		progress_veff.setValue(220);
		progress_veff.setAutoscrolls(true);
		//progress_veff.setEnabled(false);
		progress_veff.setBounds(436, 73, 18, 90);
		FluxoFundamentalPane.add(progress_veff);

		JTextPane Veff_text = new JTextPane();
		Veff_text.setText("Veff");
		Veff_text.setBounds(427, 53, 37, 21);
		FluxoFundamentalPane.add(Veff_text);
		Veff_text.setEditable(false);

		JTextPane Angulo_txt = new JTextPane();
		Angulo_txt.setText("Ângulo de Fase [ θº]");
		Angulo_txt.setBounds(729, 25, 142, 21);
		FluxoFundamentalPane.add(Angulo_txt);
		Angulo_txt.setEditable(false);

		JSeparator separator = new JSeparator();
		separator.setForeground(Color.GRAY);
		separator.setBounds(0, 175, 902, 15);
		FluxoFundamentalPane.add(separator);

		JProgressBar progress_aeff = new JProgressBar();
		progress_aeff.setMaximum(220);
		progress_aeff.setValue(39);
		progress_aeff.setStringPainted(true);
		progress_aeff.setOrientation(SwingConstants.VERTICAL);
		progress_aeff.setEnabled(false);
		progress_aeff.setBounds(436, 237, 18, 90);
		progress_aeff.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		progress_aeff.setAutoscrolls(true);
		FluxoFundamentalPane.add(progress_aeff);

		JTextPane Aeff_txt = new JTextPane();
		Aeff_txt.setText("Aeff");
		Aeff_txt.setBounds(427, 217, 37, 21);
		FluxoFundamentalPane.add(Aeff_txt);
		Aeff_txt.setEditable(false);



		JSpinner Angulo_sp2 = new JSpinner();
		JFormattedTextField tf = ((JSpinner.DefaultEditor)Angulo_sp2.getEditor()).getTextField();
		tf.setEditable(false);
		Angulo_sp2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		Angulo_sp2.setModel(new SpinnerNumberModel(value,min,max,step));
		Angulo_sp2.setBounds(754, 226, 93, 26);
		FluxoFundamentalPane.add(Angulo_sp2);


		JTextPane Angulo_txt2 = new JTextPane();
		Angulo_txt2.setText("Ângulo de Fase [ θº]");
		Angulo_txt2.setBounds(729, 202, 142, 21);
		FluxoFundamentalPane.add(Angulo_txt2);
		Angulo_txt2.setEditable(false);
		

		JTextPane txtpnAmplitude = new JTextPane();
		txtpnAmplitude.setText("Amplitude");
		txtpnAmplitude.setBounds(599, 25, 70, 21);
		FluxoFundamentalPane.add(txtpnAmplitude);
		
		JSpinner Amplitude_sp = new JSpinner();
		Amplitude_sp.setBounds(595, 48, 79, 26);
		Amplitude_sp.setModel(new SpinnerNumberModel(0,0,220,1));
		FluxoFundamentalPane.add(Amplitude_sp);
		
		JSpinner Amplitude_sp2 = new JSpinner();
		Amplitude_sp2.setBounds(595, 225, 79, 26);
		Amplitude_sp2.setModel(new SpinnerNumberModel(0,0,220,1));
		FluxoFundamentalPane.add(Amplitude_sp2);
		
		JTextPane txtpnAmplitude2 = new JTextPane();
		txtpnAmplitude2.setText("Amplitude");
		txtpnAmplitude2.setBounds(599, 202, 70, 21);
		FluxoFundamentalPane.add(txtpnAmplitude2);


		

		JTextPane Corrente_txt = new JTextPane();
		Corrente_txt.setFont(new Font("FreeSans", Font.PLAIN, 16));
		Corrente_txt.setText("Corrente");
		Corrente_txt.setBounds(182, 175, 81, 21);
		FluxoFundamentalPane.add(Corrente_txt);
		Corrente_txt.setEditable(false);

		JTextPane Tensao_txt = new JTextPane();
		Tensao_txt.setFont(new Font("FreeSans", Font.PLAIN, 16));
		Tensao_txt.setText("Tensão");
		Tensao_txt.setBounds(182, 0, 81, 21);
		FluxoFundamentalPane.add(Tensao_txt);
		Tensao_txt.setEditable(false);



		/*JButton button = new JButton("Ok");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		button.setBounds(832, 311, 58, 26);
		FluxoFundamentalPane.add(button);
		button.addActionListener(new AcaoInterfaceFluxoFundamental(PanelTensao,Angulo_sp, Angulo_sp2, Amplitude_sp, Amplitude_sp2,maxDataPoints));*/ 

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 352, 902, 9);
		FluxoFundamentalPane.add(separator_1);

		JTextPane txtpnSadas = new JTextPane();
		txtpnSadas.setFont(new Font("Liberation Sans", Font.BOLD, 22));
		txtpnSadas.setText("Saídas");
		txtpnSadas.setBounds(385, 354, 107, 32);
		FluxoFundamentalPane.add(txtpnSadas);
		txtpnSadas.setEditable(false);

		JTextPane Potenc_inst_txt = new JTextPane();
		Potenc_inst_txt.setFont(new Font("FreeSans", Font.PLAIN, 16));
		Potenc_inst_txt.setText("Potência Instantânea");
		Potenc_inst_txt.setBounds(104, 373, 215, 21);
		FluxoFundamentalPane.add(Potenc_inst_txt);
		Potenc_inst_txt.setEditable(false);



		JTextPane Text = new JTextPane();
		Text.setText("Potência Ativa");
		Text.setBounds(22, 539, 107, 21);
		FluxoFundamentalPane.add(Text);
		Text.setEditable(false);

		Text_Pont_Atv = new JTextField();
		Text_Pont_Atv.setHorizontalAlignment(SwingConstants.TRAILING);
		Text_Pont_Atv.setBounds(182, 541, 137, 23);
		FluxoFundamentalPane.add(Text_Pont_Atv);
		Text_Pont_Atv.setColumns(10);
		Text_Pont_Atv.setEditable(false);

		Text_Pot_Rea = new JTextField();
		Text_Pot_Rea.setColumns(10);
		Text_Pot_Rea.setBounds(182, 574, 137, 23);
		FluxoFundamentalPane.add(Text_Pot_Rea);
		Text_Pot_Rea.setEditable(false);

		JTextPane txtpnPotnciaReativa = new JTextPane();
		txtpnPotnciaReativa.setText("PotxtpnPotnciaReativa tência Reativa");
		txtpnPotnciaReativa.setBounds(22, 572, 148, 21);
		FluxoFundamentalPane.add(txtpnPotnciaReativa);
		txtpnPotnciaReativa.setEditable(false);

		Text_Pot_Apa = new JTextField();
		Text_Pot_Apa.setColumns(10);
		Text_Pot_Apa.setBounds(182, 599, 137, 23);
		FluxoFundamentalPane.add(Text_Pot_Apa);
		Text_Pot_Apa.setEditable(false);

		JTextPane txtpnPotnciaAparente = new JTextPane();
		txtpnPotnciaAparente.setText("Potência Aparente");
		txtpnPotnciaAparente.setBounds(22, 597, 132, 21);
		FluxoFundamentalPane.add(txtpnPotnciaAparente);
		txtpnPotnciaAparente.setEditable(false);

		Text_Fator_Pot = new JTextField();
		Text_Fator_Pot.setColumns(10);
		Text_Fator_Pot.setBounds(182, 630, 137, 23);
		FluxoFundamentalPane.add(Text_Fator_Pot);
		Text_Fator_Pot.setEditable(false);

		JTextPane txtpnFator = new JTextPane();
		txtpnFator.setText("Fator de Potência");
		txtpnFator.setBounds(22, 628, 161, 25);
		FluxoFundamentalPane.add(txtpnFator);
		txtpnFator.setEditable(false);
		
		
		List<Double> scores1 = new ArrayList<>();
		int maxDataPoints = 50;
		GraphPanel PanelTensao = new model.GraphPanel(scores1);
		PanelTensao.setOpaque(false);
		PanelTensao.setBorder(null);
		PanelTensao.setBounds(12, 32, 399, 132);
		PanelTensao.setBackground(new Color(238,238,238));
		FluxoFundamentalPane.add(PanelTensao);
		
		List<Double> scores2 = new ArrayList<>();
		GraphPanel Panelcorrente = new model.GraphPanel(scores2);
		Panelcorrente.setOpaque(false);
		Panelcorrente.setBorder(null);
		Panelcorrente.setBounds(12, 202, 399, 132);
		Panelcorrente.setBackground(new Color(238,238,238));
		FluxoFundamentalPane.add(Panelcorrente);
		
		List<Double> scores3 = new ArrayList<>();
		GraphPanel PanelPotenciaInst = new model.GraphPanel(scores3);
		PanelPotenciaInst.setOpaque(false);
		PanelPotenciaInst.setBorder(null);
		PanelPotenciaInst.setBounds(12, 397, 399, 132);
		PanelPotenciaInst.setBackground(new Color(238,238,238));
		FluxoFundamentalPane.add(PanelPotenciaInst);
		
		List<Double> scores4 = new ArrayList<>();
		
		GraphPanel TrianPontencia = new model.GraphPanel(scores4);
		TrianPontencia.setOpaque(false);
		TrianPontencia.setBorder(null);
		TrianPontencia.setBounds(609, 406, 249, 236);
		TrianPontencia.setBackground(new Color(238,238,238));
		FluxoFundamentalPane.add(TrianPontencia);
		
		
		JButton btnOk = new JButton("Ok");
		btnOk.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});
		btnOk.setBounds(832, 132, 58, 26);
		FluxoFundamentalPane.add(btnOk);
		btnOk.addActionListener(new AcaoInterfaceFluxoFundamental(PanelTensao,Panelcorrente,PanelPotenciaInst,TrianPontencia,Angulo_sp, Angulo_sp2, Amplitude_sp, Amplitude_sp2,maxDataPoints)); 

	}
}
