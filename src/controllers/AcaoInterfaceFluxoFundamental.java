package controllers;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import view.FluxoPotenciaFundamental;
import model.GraphPanel;

public class AcaoInterfaceFluxoFundamental implements ActionListener{
  

	private double Potencia_Ativa;
	private double Potencia_Reativa;
	private double Potencia_Aparente;
	private double Fator_Potencia;
	private int AnguloFase1;
	private int AnguloFase2;
	private int AmplitudeTensao;
	private int AmplitudeCorrente;
	private JSpinner Angulo_sp;
	private JSpinner Angulo_sp2;
	private JSpinner Amplitude_sp;
	private JSpinner Amplitude_sp2;
	private int maxDataPoints;
	private GraphPanel PanelTensao;
	private GraphPanel Panelcorrente;
	private GraphPanel PanelPotenciaInst;
	private GraphPanel TrianPontencia;
	
	
  public AcaoInterfaceFluxoFundamental(GraphPanel PanelTensao,GraphPanel Panelcorrente,GraphPanel PanelPotenciaInst,GraphPanel TrianPontencia,JSpinner Angulo_sp,JSpinner Angulo_sp2,JSpinner Amplitude_sp,JSpinner Amplitude_sp2,int maxDataPoints) {
	  this.PanelTensao = PanelTensao;
	  this.Panelcorrente = Panelcorrente;
	  this.PanelPotenciaInst= PanelPotenciaInst;
	  this.TrianPontencia = TrianPontencia;
	  this.Angulo_sp = Angulo_sp;
	  this.Angulo_sp2 = Angulo_sp2;
	  this.Amplitude_sp = Amplitude_sp;
	  this.Amplitude_sp2 = Amplitude_sp2;
	  this.maxDataPoints = maxDataPoints;
	 
  }


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String command = e.getActionCommand();
		if(command.equals("Ok")) {
			AnguloFase1 = (int) Angulo_sp.getValue();
			AnguloFase2 = (int) Angulo_sp2.getValue();
			AmplitudeTensao = (int) Amplitude_sp.getValue();
			AmplitudeCorrente = (int) Amplitude_sp2.getValue();
			
			if(AnguloFase1 > 180 || AnguloFase1 < -180 || AnguloFase2 > 180 || AnguloFase2 < -180) {
				JOptionPane.showMessageDialog(null, "Algum Ângulo está errado");
			}
			if(AmplitudeTensao > 220 || AmplitudeTensao < 0 || AmplitudeCorrente > 220 || AmplitudeCorrente < 0) {
				JOptionPane.showMessageDialog(null, "Alguma Amplitude está incorreta");
			}
			
			Potencia_Ativa = (int)(AnguloFase2*AnguloFase1*((float)Math.cos(Math.toRadians(AnguloFase1-AnguloFase2))));
			Potencia_Reativa = (int)(AnguloFase2*AnguloFase1*((float)Math.cos(Math.toRadians(AnguloFase1-AnguloFase2))));
			Potencia_Aparente = (int)(AnguloFase2*AnguloFase1);
			Fator_Potencia = (float)Math.cos((Math.toRadians(AnguloFase1-AnguloFase2)));
			
			List<Double> scores1 = new ArrayList<>();
			
			for(double i = 0;i < maxDataPoints;i++) {
				scores1.add(i);
			}
			PanelTensao.setScores(scores1);
			
			List<Double> scores2 = new ArrayList<>();
			
			for(double i = 0;i < maxDataPoints;i++) {
				scores2.add(i);
			}
			Panelcorrente.setScores(scores2);
			List<Double> scores3 = new ArrayList<>();
			
			for(double i = 0;i < maxDataPoints;i++) {
				scores3.add(i);
			}
			PanelPotenciaInst.setScores(scores3);
			List<Double> scores4 = new ArrayList<>();
			
			for(double i = 0;i < maxDataPoints;i++) {
				scores4.add(i);
			}
			TrianPontencia.setScores(scores4);
			

		}
		
		view.FluxoPotenciaFundamental.Text_Pont_Atv.setText(String.valueOf(Potencia_Ativa)+" 	Watt");
		view.FluxoPotenciaFundamental.Text_Pot_Rea.setText(String.valueOf(Potencia_Reativa)+" 	VAR");
		view.FluxoPotenciaFundamental.Text_Pot_Apa.setText(String.valueOf(Potencia_Aparente)+" 	VA");
		view.FluxoPotenciaFundamental.Text_Fator_Pot.setText(String.valueOf(Fator_Potencia));
		
		
		
		
	}

}
