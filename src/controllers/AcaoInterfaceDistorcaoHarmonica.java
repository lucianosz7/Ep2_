package controllers;

import java.awt.Color;
import java.awt.event.ActionEvent;

import view.DistorcaoHarmonica;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import model.GraphPanel;
import view.DistorcaoHarmonica;

public class AcaoInterfaceDistorcaoHarmonica implements ActionListener{

	private JSpinner Angulo_sp;
	private JSpinner NumOrdHarmonicas;
	private JSpinner Angulo_sp2 ;
	private JSpinner OrdemHar_sp;
	private JSpinner Angulo_Sp3 ;
	private JSpinner OrdemHar_sp2;
	private int AnguloFase1;
	private int NumOrdHar;
	private int AnguloFase2;
	private int OrdemHar1;
	private int AnguloFase_3;
	private int OrdemHar2;
	private int maxDataPoints;
	private GraphPanel ComponenteFundamentalPane;
	private GraphPanel PanelHarm1;
	private GraphPanel PanelHarm2;
	private GraphPanel PanelResultante;
	private JTextPane AnguloFase_1;
	private JTextPane AnguloFase3;
	private JTextPane txtpnOrdemHarmnica2;
	private JTextPane txtpnOrdemHarmnica3;
	private JProgressBar progressAmp_Har2;
	private JProgressBar progressAmp_Har3;
	private JTextPane AmplitudeText1;
	private JTextPane AmplitudeText2;
	
	public AcaoInterfaceDistorcaoHarmonica(JSpinner Angulo_sp,JSpinner Angulo_sp2,JSpinner Angulo_Sp3,JSpinner OrdemHar_sp,JSpinner OrdemHar_sp2,JSpinner NumOrdHarmonicas,GraphPanel ComponenteFundamentalPane,GraphPanel PanelHarm1,GraphPanel PanelHarm2,GraphPanel PanelResultante,JTextPane txtpnOrdemHarmnica2,JTextPane txtpnOrdemHarmnica3,JProgressBar progressAmp_Har2,JProgressBar progressAmp_Har3,JTextPane AmplitudeText1,JTextPane AmplitudeText2,JTextPane AnguloFase_1,JTextPane AnguloFase3,int maxDataPoints) throws IOException {
		
		this.Angulo_sp = Angulo_sp;
		this.Angulo_sp2 = Angulo_sp2;
		this.Angulo_Sp3 = Angulo_Sp3;
		this.OrdemHar_sp = OrdemHar_sp;
		this.OrdemHar_sp2 = OrdemHar_sp2;
		this.NumOrdHarmonicas = NumOrdHarmonicas;
		this.ComponenteFundamentalPane = ComponenteFundamentalPane;
		this.PanelHarm1 = PanelHarm1;
		this.PanelHarm2 = PanelHarm2;
		this.PanelResultante = PanelResultante;
		this.txtpnOrdemHarmnica2 = txtpnOrdemHarmnica2;
		this.txtpnOrdemHarmnica3 = txtpnOrdemHarmnica3;
		this.progressAmp_Har2 = progressAmp_Har2;
		this.progressAmp_Har3 = progressAmp_Har3;
		this.AmplitudeText1 = AmplitudeText1;
		this.AmplitudeText2= AmplitudeText2;
		this.AnguloFase_1 = AnguloFase_1;
		this.AnguloFase3 = AnguloFase3;
		this.maxDataPoints = maxDataPoints;
	}
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String command = e.getActionCommand();
		if(command.equals("Ok")) {

			AnguloFase1 = (int) Angulo_sp.getValue();
			AnguloFase2 = (int) Angulo_sp2.getValue();
			AnguloFase_3 = (int) Angulo_Sp3.getValue();
			NumOrdHar = (int) NumOrdHarmonicas.getValue();
			/*OrdemHar1 = (int) OrdemHar_sp.getValue();
			OrdemHar2 = (int) OrdemHar_sp2.getValue();*/
			
			if(AnguloFase1 > 180 || AnguloFase1 < -180 || AnguloFase2 > 180 || AnguloFase2 < -180) {
				JOptionPane.showMessageDialog(null, "Algum Ângulo está errado");
			}
			if(NumOrdHar > 6 || NumOrdHar < 0) {
				JOptionPane.showMessageDialog(null, "Algum Número d Harmônicos está errado");
			}
			if(OrdemHar1 < 0 || OrdemHar1 > 15 || OrdemHar2 < 0 || OrdemHar2 > 15) {
				JOptionPane.showMessageDialog(null, "Alguma Ordem Harmônica está errada");
			}
			
			
			
			//Adiciona os dados para plotar o gráfico
			List<Double> scores = new ArrayList<>();
			
			for(double i = 0;i < maxDataPoints;i++) {
				scores.add(i);
			}
			ComponenteFundamentalPane.setScores(scores);
			
			List<Double> scores2 = new ArrayList<>();
			
			for(double i = 0;i < maxDataPoints;i++) {
				scores2.add(i);
			}
			PanelHarm1.setScores(scores2);
			
			
			List<Double> scores3 = new ArrayList<>();
			
			for(double i = 0;i < maxDataPoints;i++) {
				scores3.add(i);
			}
			PanelHarm2.setScores(scores3);
			
			List<Double> scores4 = new ArrayList<>();
			
			for(double i = 0;i < maxDataPoints;i++) {
				scores4.add(i);
			}
			PanelResultante.setScores(scores4);
		
		
			if(NumOrdHar == 0) {
				Angulo_sp2.setVisible(false);
				OrdemHar_sp.setVisible(false);
				Angulo_Sp3.setVisible(false);
				OrdemHar_sp2.setVisible(false);
				//AnguloFase_1.setVisible(false);
				AnguloFase3.setVisible(false);
				txtpnOrdemHarmnica2.setVisible(false);
				txtpnOrdemHarmnica3.setVisible(false);
				progressAmp_Har2.setVisible(false);
				progressAmp_Har3.setVisible(false);
				AmplitudeText1.setVisible(false);
				AmplitudeText2.setVisible(false);
				PanelResultante.setVisible(false);
			}
			
			
			if(NumOrdHar == 1) {
				
				PanelHarm1.setVisible(true);
				Angulo_sp2.setVisible(true);
				AnguloFase_1.setVisible(true);
				//AnguloFase3.setVisible(true);
				OrdemHar_sp.setVisible(true);
				txtpnOrdemHarmnica2.setVisible(true);
				progressAmp_Har2.setVisible(true);
				AmplitudeText1.setVisible(true);
				txtpnOrdemHarmnica2.setVisible(true);
				PanelResultante.setVisible(true);
				OrdemHar1 = (int) OrdemHar_sp.getValue();
				OrdemHar2 = (int) OrdemHar_sp2.getValue();
				
				
			}
			if(NumOrdHar == 2) {
				PanelHarm2.setVisible(true);
				Angulo_Sp3.setVisible(true);
				//AnguloFase_1.setVisible(true);
				AnguloFase3.setVisible(true);
				OrdemHar_sp2.setVisible(true);
				txtpnOrdemHarmnica3.setVisible(true);
				progressAmp_Har3.setVisible(true);
				AmplitudeText2.setVisible(true);
				//AmplitudeText2.setVisible(true);
				txtpnOrdemHarmnica3.setVisible(true);
				PanelResultante.setVisible(true);
				OrdemHar1 = (int) OrdemHar_sp.getValue();
				OrdemHar2 = (int) OrdemHar_sp2.getValue();
				
			}
			
		
			 
			 
			
			}
		}
	}

