package controllers;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;

import javax.swing.JButton;

import view.DistorcaoHarmonica;
import view.FluxoPotenciaFundamental;

public class AcaoAprendaQee {
	
	private JButton btnDistoroHarmonica;
	
	public AcaoAprendaQee(JButton btnDistoroHarmonica) {
		this.btnDistoroHarmonica = btnDistoroHarmonica;
	}
	
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		if(command.equals("Fluxo de Potência Fundamental")){
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						FluxoPotenciaFundamental frame = new FluxoPotenciaFundamental();
						frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
		else if(command.equals("Distorção Harmonica")) {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						DistorcaoHarmonica frame = new DistorcaoHarmonica();
						frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
						}
					}

				});
			}
		
		}
		
	}

