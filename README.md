Nome: Luciano Dos Santos Silva
Matricula: 16/0013321

O programa Inicia composto de 3 Opções:
1- Simular o Fluxo de Potência Fundamental.
2- Simular Distorção Harmônica.
3- Sair.

Quando a primeira Opção é selecionada aparece uma nova Janela, nesta janela 
contém as opções de dados para serem inseridos. Quando o Usuário estiver 
pronto ele irá apertar "Ok" e a simulação esperada ocorrerá.E quando o usuario
fecha a janela da simulação ele volta para o menu inicial.
O mesmo vale para o caso da Simulação da Distorção Harmônica.Quando o usuário
aperta o sair o programa é finalizado.

Para executar o Programa basta clonar o respositorio ou fazer o download
e abrir com o Eclipse.

